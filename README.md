# world_time_app

world_time_app is an application that shows you the current local time of a given timezone.

## Table of contents
* [Technologies](#markdown-header-technologies)
* [Installation](#markdown-header-installation)
* [Features](#markdown-header-features)
* [Inspiration](#markdown-header-inspiration)

## Technologies
Project is created with:

* Flutter version: 2.2.3
* Dart version: 2.13.4
* http package version: 0.13.3
* intl package version: 0.17.0
* flutter_spinkit package version: 5.0.0

## Installation
1. Make sure you installed flutter on your system.
2. Pull down the code locally.
3. Start Android Studio.
4. Install the Flutter and Dart plugins if not exist. Open plugin preferences (File > Settings > Plugins).
5. Select Marketplace, select the Flutter plugin and click Install.
6. Click Yes when prompted to install the Dart plugin.
7. Click Restart when prompted.
8. Open Android Studio and select 'Open an existing Android Studio Project'
9. From the terminal:
   Run
   ```sh
     flutter pub get
   ```	 
   OR
   From Android Studio/IntelliJ: Click Packages get in the action ribbon at the top of pubspec.yaml.
10. Run the application on your emulator or local device.

## Screenshots
![home_screen_day](screenshots/home_screen_day.jpg){height=1350 width=1080}
![home_screen_night](screenshots/home_screen_night.jpg){height=1350 width=1080}
![choose_location_screen](screenshots/choose_location_screen.jpg){height=1350 width=1080}

## Features
* Get the local time for a given timezone.

## Inspiration
This app is based on Flutter Tutorial for beginners From [The Net Ninja](https://github.com/iamshaunjp/flutter-beginners-tutorial/tree/lesson-35/world_time_app) 


