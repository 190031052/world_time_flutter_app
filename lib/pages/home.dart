import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Map data={};

  @override
  Widget build(BuildContext context) {

    data=data.isNotEmpty?data:ModalRoute.of(context)!.settings.arguments as Map;

    String bgImage=data['isDayTime']?'day.png':'night.png';

    Color? bgColor=data['isDayTime']?Color(0xff045f9b):Color(0xff07132a);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
        backgroundColor: bgColor,
        body: SafeArea(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/$bgImage'),
                  fit:BoxFit.cover
                )
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0,112.0,0.0,0.0),
                child: Column(
                  children: [
                    FlatButton.icon(
                        onPressed: () async{
                          dynamic result=await Navigator.pushNamed(context,'/location');
                          setState(() {
                            data={
                              'location':result['location'],
                              'time':result['time'],
                              'flag':result['flag'],
                              'isDayTime':result['isDayTime']
                            };
                          });
                        },
                        icon: Icon(
                            Icons.location_pin,
                          color: Colors.grey[200],
                        ),
                        label: Text(
                            "Choose Location",
                          style: TextStyle(
                            color: Colors.grey[200]
                          ),
                        )
                    ),
                    SizedBox(height: 20.0,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          data['location'],
                          style: TextStyle(
                            fontSize: 25.0,
                            letterSpacing: 2.0,
                            color: Colors.white
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 20.0,),
                    Text(
                        data['time'],
                      style: TextStyle(
                        fontSize: 50.0,
                        color: Colors.white
                      ),
                    )
                  ],
                ),
              ),
            )
        ),
      ),
    );
  }
}
