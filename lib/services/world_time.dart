import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';


class WorldTime{

  String location; //location for the UI
  late String time; //time in that location
  String flag; //url to asset flag
  String url; //location url api endpoint
  late bool isDayTime; //is day time or night time

  WorldTime({required this.location,required this.flag,required this.url});


  Future<void> getTime() async{

    try {
      var uri = Uri.parse('http://worldtimeapi.org/api/timezone/${this.url}');
      Response response = await get(uri);
      Map data = jsonDecode(response.body);

      String datetime = data['datetime'];
      String offset_sign=data['utc_offset'][0];
      String offset_h = data['utc_offset'].substring(1, 3);
      String offset_m=data['utc_offset'].substring(4);

      DateTime now = DateTime.parse(datetime);

      if (offset_sign=="+"){
        now = now.add(Duration(hours: int.parse(offset_h),minutes: int.parse(offset_m)));
      }
      else{
        now = now.subtract(Duration(hours: int.parse(offset_h),minutes: int.parse(offset_m)));
      }


      this.isDayTime=now.hour>6&&now.hour<20 ? true:false;

      this.time = DateFormat.jm().format(now);

    }catch(e){
      this.time="Could not load time";
    }
  }
}