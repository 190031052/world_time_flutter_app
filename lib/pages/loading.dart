import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:world_time/services/world_time.dart';

class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {


  void setUpWorldTime() async{
    WorldTime worldTime= WorldTime(location: 'Kolkata', flag: 'india.png', url: 'Asia/Kolkata');
    await worldTime.getTime();
    Navigator.pushReplacementNamed(context, '/home',arguments: {
      'location':worldTime.location,
      'time':worldTime.time,
      'flag':worldTime.flag,
      'isDayTime':worldTime.isDayTime
    });


  }

  @override
  void initState() {
    super.initState();
    setUpWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
        backgroundColor: Colors.blue[900],
        body: Center(
          child: SpinKitThreeBounce(
            color: Colors.white,
            size: 25.0,
          ),
        )
      ),
    );
  }
}
